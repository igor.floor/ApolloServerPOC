const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const url = require('url');
const { createServer } = require('http');
const { SubscriptionServer } = require('subscriptions-transport-ws');
const { PubSub } = require('graphql-subscriptions');

const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
const { execute, subscribe } = require('graphql');

const createGraphQLLogger = require('graphql-log');
const logExecutions = createGraphQLLogger();

const fs = require('fs');
const pubsub = new PubSub();
// -------------------Kafka things-----------//
//Kafka producer part
var kafka = require("kafka-node");
var Producer = kafka.Producer,
    client = new kafka.Client('172.16.2.140:2181'),
    producer = new Producer(client);

producer.on('ready', function () {
    console.log("Producer is ready");
});

producer.on('error', function (err) {
    console.log("Producer is in error state");
    console.log(err);
});

//Kafka consumer part
consumer = new kafka.Consumer(client,
    [ { topic: 'topicName', offset: 0}],
    {
        groupId: 'kafka-node-group',//consumer group id, default `kafka-node-group`
        // Auto commit config
        autoCommit: true,
        autoCommitIntervalMs: 5000,
        // The max wait time is the maximum amount of time in milliseconds to block waiting if insufficient data is available at the time the request is issued, default 100ms
        fetchMaxWaitMs: 100,
        // This is the minimum number of bytes of messages that must be available to give a response, default 1 byte
        fetchMinBytes: 1,
        // The maximum bytes to include in the message set for this partition. This helps bound the size of the response.
        fetchMaxBytes: 1024 * 1024,
        // If set true, consumer will fetch message from the given offset in the payloads
        fromOffset: false,
        // If set to 'buffer', values will be returned as raw buffer objects.
        encoding: 'utf8'
    }
);

consumer.on('message', function (message) {
    console.log("uou: message"+JSON.stringify(message));
});

consumer.on('error', function (err) {
    console.log('Error:',err);
})

consumer.on('offsetOutOfRange', function (err) {
    console.log('offsetOutOfRange:',err);
})

// -------------------Kafka things-----------//


const resolvers = require('./graphql/books/resolver')(pubsub, consumer);
logExecutions(resolvers);


// Put together a schema
const schema = makeExecutableSchema({
        typeDefs: fs.readFileSync('./graphql/books/schema.graphql').toString(),
        resolvers,
});

// Initialize the app
const server = express();

const PORT = process.env.PORT || 3000;

// The GraphQL endpoint
server.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

//New way that makes the subscriptions path be dynamic
server.use('/graphiql', graphiqlExpress(req => ({
    endpointURL: '/graphql',
    subscriptionsEndpoint: url.format({
        host: req.get('host'),
        protocol: req.protocol === 'https' ? 'wss' : 'ws',
        pathname: '/subscriptions'
    })
})));

server.post('/sendMsg',function(req,res){
    console.log("Received: "+JSON.stringify(req.body));
    var body = {
        topic: 'topicName',
        messages: ['message body'], // multi messages should be a array, single message can be just a string or a KeyedMessage instance
        key: 'theKey', // only needed when using keyed partitioner (optional)
        partition: 0, // default 0 (optional)
        attributes: 2 // default: 0 used for compression (optional)
    };
    var sentMessage = JSON.stringify(body.messages);
    payloads = [
        { topic: body.topic, messages:sentMessage , partition: 0 }
    ];
    producer.send(payloads, function (err, data) {
        console.log("sent");
        res.json(data);
    });

});

const ws = createServer(server);
ws.listen(PORT, () => {
    console.log("Apollo server is running on http://localhost:"+PORT+"");
    new SubscriptionServer({
        execute,
        subscribe,
        schema
    }, {
        server:ws,
        path: '/subscriptions',
        });
});

// // Start the server
// server.listen(3000, () => {
//     console.log('Go to http://localhost:3000/graphiql to run queries!');
// });
