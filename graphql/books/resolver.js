const { withFilter }= require('graphql-subscriptions');

let links = [
    {
    id: 'link-0',
    url: 'www.howtographql.com',
    description: 'Fullstack tutorial for GraphQL'
    },
    {
        id: 'link-1',
        url: 'www.apolloServer.com',
        description: 'Nice apollo!'
    }
]

findLinkByID = function(id) {
    var link = null;
    links.forEach(function (linkInArray, index) {
        if (linkInArray.id == id) {
            link = linkInArray;
        }
    });
    return link;
}

findLinkIndexByID = function(id) {
    var indexReturn = -1;
    links.forEach(function (linkInArray, index) {
        if (linkInArray.id == id) {
            indexReturn = index;
        }
    });
    return indexReturn;
}

let idCount = links.length;

function myPromise(ms, callback) {
    return new Promise(function(resolve, reject) {
        // Set up the real work
        callback(resolve, reject);

        // Set up the timeout
        setTimeout(function() {
            reject('Promise timed out after ' + ms + ' ms');
        }, ms);
    });
}

async function waitForResponse() {
    var responsePromise = new Promise(function(resolve, reject) {
        consumer.on('message', function (message) {
            console.log("uou, received from kafka: message"+JSON.stringify(message));
            resolve(JSON.stringify(message));
        });

        setTimeout(function() {
            reject('Promise waiting for kafka timed out');
        }, 20000);
    });

    var returnValue = "nothing";
    await responsePromise.then(result => {
        console.log("Then on promise");
        returnValue = result;
    }).catch(error => {
        console.log("Error on promise");
        returnValue = "Waiting for kafka and it timed out";
    });
    console.log("Passing...");

    return returnValue;
}

module.exports = function(pubsub, kafkaConsumer) {

        return {
            Query: {
                info: () => `This is the api of the Hackernews Clone`,

                feed: () => links,
                link: (root, args) => {
                    return findLinkByID(args.id);
                },
                waitingForKafka: () => {
                    var returnFromWait = waitForResponse();
                    console.log("Return from wait: "+returnFromWait);
                    return returnFromWait;
                }
            },

            Mutation: {
                post: (root, args) => {
                    const link = {
                        id: `link-${idCount++}`,
                        description: args.description,
                        url: args.url,
                    }
                    links.push(link);
                    pubsub.publish('createdLink', {createdLink: link});
                    return link
                },

                updateLink: (root, args) => {
                    link = findLinkByID(args.id);
                    if (link != null) {
                        link.description = args.description;
                        link.url = args.url;
                        pubsub.publish('linkChanged', {linkChanged: link});
                        pubsub.publish('specificLinkChanged', {specificLinkChanged: link});
                        return link;
                    } else {
                        return null;
                    }
                },

                deleteLink: (root, args) => {
                    var linkIndex = findLinkIndexByID(args.id);
                    var link = null;
                    if (linkIndex >= 0) {
                        link = links[linkIndex];
                        links.splice(linkIndex, 1);
                        pubsub.publish('linkDeleted', {linkDeleted: link});
                        pubsub.publish('specificLinkDeleted', {specificLinkDeleted: link});
                        return link;
                    } else {
                        return null;
                    }


                }

            },
            Subscription: {
                createdLink: {
                    subscribe: () => pubsub.asyncIterator('createdLink')
                },
                linkChanged: {
                    subscribe: () => pubsub.asyncIterator('linkChanged')
                },
                specificLinkChanged: {
                    subscribe: withFilter(() => pubsub.asyncIterator('specificLinkChanged'), (payload, variables) => {
                        return payload.specificLinkChanged.id === variables.id;
                    }),
                },
                linkDeleted: {
                    subscribe: () => pubsub.asyncIterator('linkDeleted')
                },
                specificLinkDeleted: {
                    subscribe: withFilter(() => pubsub.asyncIterator('specificLinkDeleted'), (payload, variables) => {
                        return payload.specificLinkDeleted.id === variables.id;
                    }),
                },

            },
        }


}